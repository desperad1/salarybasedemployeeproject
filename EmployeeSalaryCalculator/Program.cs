﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeSalaryCalculator
{
    public interface IEmployeeViewData
    {
         string Name { get; set; }
         string Surname { get; set; }
         string TCKN { get; set; }
         double Salary { get; set; }
    }
    public class EmployeeViewData : IEmployeeViewData
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TCKN { get; set; }
        public double Salary { get; set; }
        public EmployeeViewData(string name,string surname,string tckn,double salary)
        {
            this.Name = name;
            this.Surname = surname;
            this.TCKN = tckn;
            this.Salary = salary;
        }
    }
    public abstract class BaseEmployee
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TCKN { get; set; }
        
        public abstract double CalculateSalary();
        public  IEmployeeViewData GetEmployeeData()
        {
            return new EmployeeViewData(this.Name,this.Surname,this.TCKN,CalculateSalary());
            
        }

        public BaseEmployee(string name, string surname, string tckn)
        {
            this.Name = name;
            this.Surname = surname;
            this.TCKN = tckn;

        }
        

    }
    public class FixedSalaryEmployee : BaseEmployee
    {
        public double calculatedSalary { get; set; }
        public FixedSalaryEmployee(string name, string surname, string tckn, double salary) : base(name, surname, tckn)
        {
            calculatedSalary = salary;
        }
        public override double CalculateSalary()
        {
            return calculatedSalary;
        }

    }
    public class DailyBasedSalaryEmployee : BaseEmployee
    {
        private double _daysWorked;
        public double DaysWorked {
            get { return _daysWorked; } 
            set {
                if (_daysWorked < 0 || _daysWorked > 31) {
                    throw new Exception("Çalışılan gün 0-31 aralığında olmalıdır.");
                }
                _daysWorked = value; } }
        public double DailyRate { get; set; }
        public DailyBasedSalaryEmployee(string name, string surname, string tckn, double daysWorked, double dailyRate) : base(name, surname, tckn)
        {
            this.DaysWorked = daysWorked;
            this.DailyRate = dailyRate;
        }
        public override double CalculateSalary()
        {
            return DaysWorked * DailyRate;
        }

    }
    public class OvertimeSalaryEmployee : BaseEmployee
    {
        public double BaseSalary { get; set; }
        public double OvertimeHours { get; set; }
        private double _overtimeRate;
        public double OverTimeRate
        {
            get { return _overtimeRate; }
            set
            {
                if (value < 0){
                    throw new Exception("Mesai ücreti 0dan düşük olamaz.");
                }
                _overtimeRate = value;
            }
        }
        public OvertimeSalaryEmployee(string name, string surname, string tckn, double baseSalary, double overtimeHours, double overtimeRate)
            : base(name, surname, tckn)
        {
            BaseSalary = baseSalary;
            OvertimeHours = overtimeHours;
            OverTimeRate = overtimeRate;
        }
        public override double CalculateSalary()
        {
            return BaseSalary + (OvertimeHours * OverTimeRate);
        }

    }
   
    public interface IEmployeeSalaryCalculationService
    {
        IEmployeeViewData FindEmployeeByTCKN(string tckn);
        List<IEmployeeViewData> GetEmployees();
        
    }
    public class EmployeeSalaryCalculationService : IEmployeeSalaryCalculationService
    {
        public List<BaseEmployee> employees = new List<BaseEmployee>()
        {
            new FixedSalaryEmployee("emir","elkabes","12345678901",500),
            new DailyBasedSalaryEmployee("ali","veli","40632343011",25,5),
            new OvertimeSalaryEmployee("can","ali","12300034598",200,8,20)
        };
        public IEmployeeViewData FindEmployeeByTCKN(string tckn)
        {
            return employees.FirstOrDefault(t => t.TCKN ==tckn).GetEmployeeData();
            
        }

        public List<IEmployeeViewData> GetEmployees()
        {
            return employees.Select(t => t.GetEmployeeData()).ToList();
            
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            IEmployeeSalaryCalculationService serv = new EmployeeSalaryCalculationService();
            serv.GetEmployees().ForEach(t => Console.WriteLine(t.Name + " " + t.Surname + " " + t.TCKN + " " + t.Salary));
            Console.ReadLine();
        }
    }
}
